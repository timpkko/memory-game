﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memory_Game
{
    class LeaderboardLoad
    {
        private string time;
        private string playerName;
        private int level;
        private List<LeaderboardLoad> list = new List<LeaderboardLoad>();

        public LeaderboardLoad(string line)
        {
            string[] separateLine = line.Split(',');
            time = separateLine[0];
            playerName = separateLine[1];
            level = int.Parse(separateLine[2]);
        }

        public LeaderboardLoad(string[] lines)
        {
            foreach (string line in lines)
            {
                list.Add(new LeaderboardLoad(line));
            }
        }

        public string[] getThreeBestBlayers()
        {
            list.Sort((x, y) => (TimeSpan.Parse(x.time).TotalSeconds).CompareTo(TimeSpan.Parse(y.time).TotalSeconds));
            string[] topThree = new string[3];
            for (int i = 0; i < topThree.Length; i++)
            {
                topThree[i] = list[i].playerName + " " + list[i].time;
            }
            return topThree;
        }
    }
}
