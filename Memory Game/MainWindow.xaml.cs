﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Memory_Game
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<BitmapImage> imgFileList;
        List<Image> imgBoxList;
        List<Button> imgBtnList;
        bool[] disabledButtons = new bool[16];
        bool isFirstButton = true;
        Button firstButton;
        Image firstImg;
        int levelMilliseconds = 1500;
        DispatcherTimer timer;
        int addSeconds = 1;


        public MainWindow()
        {
            InitializeComponent();

            // import the images files as a BitmapImage object in a list
            imgFileList = new List<BitmapImage>
            {
                new BitmapImage(new Uri("birb.jpg", UriKind.Relative)),
                new BitmapImage(new Uri("bunny.jpg", UriKind.Relative)),
                new BitmapImage(new Uri("cat.jpg", UriKind.Relative)),
                new BitmapImage(new Uri("hamster.jpg", UriKind.Relative)),
                new BitmapImage(new Uri("kiwi.jpg", UriKind.Relative)),
                new BitmapImage(new Uri("panda.jpg", UriKind.Relative)),
                new BitmapImage(new Uri("pug.jpg", UriKind.Relative)),
                new BitmapImage(new Uri("rat.jpg", UriKind.Relative))
            };

            // create a list with the image boxes that are set on the buttons
            imgBoxList = new List<Image>
            {
                img1, img2, img3, img4, img5, img6, img7, img8, img9, img10, img11, img12, img13, img14, img15, img16
            };

            // create a list with the buttons in the grid
            imgBtnList = new List<Button>
            {
                imgBtn1, imgBtn2, imgBtn3, imgBtn4, imgBtn5, imgBtn6, imgBtn7, imgBtn8, imgBtn9, imgBtn10, imgBtn11, imgBtn12, imgBtn13, imgBtn14, imgBtn15, imgBtn16
            };

            // check if the text file exists
            // if it doesn't a new file will be created
            // if it does, it will load the file and display the top three players
            if (!File.Exists("players.txt")) File.Create("players.txt");
            else LoadBestPlayers();
        }

        private void LoadBestPlayers()
        {
            string[] lines = File.ReadAllLines("players.txt");
            LeaderboardLoad lb = new LeaderboardLoad(lines);
            string[] bestPlayers = lb.getThreeBestBlayers();
            RankOnePlayerLbl.Content = "1. " + bestPlayers[0];
            RankTwoPlayerLbl.Content = "2. " + bestPlayers[1];
            RankThreePlayerLbl.Content = "3. " + bestPlayers[2];
        }

        private void levelDiffcultySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // set how long the images will display when the player made a wrong match
            // depending on the level of difficulty
            if (e.NewValue == 1) levelMilliseconds = 1500;
            if (e.NewValue == 2) levelMilliseconds = 1000;
            if (e.NewValue == 3) levelMilliseconds = 750;
            string msg = string.Format("Level: {0}", e.NewValue);
            levelLbl.Content = msg;
        }

        private async void IsMatching(Image img, Button imgBtn)
        {
            // check if the button is the first that the player clicked on using a bool
            // if yes, bool is false and awaits for the second button to be clicked
            if (isFirstButton)
            {
                firstButton = imgBtn;
                firstImg = img;
                imgBtn.IsEnabled = false;
                isFirstButton = false;
            }
            // check if the images on the two buttons clicked match
            else if (img.Source == firstImg.Source)
            {
                imgBtn.IsEnabled = false;
                // disabledButtons is a bool array that keeps track of the buttons.
                // whenever the player has made a match, the element of the button clicked in the array
                // will be set to true, meaning the images at those buttons will stay opened
                for (int i = 0; i < imgBtnList.Capacity; i++)
                {
                    if (imgBtn == imgBtnList[i] || firstButton == imgBtnList[i]) disabledButtons[i] = true;
                    continue;
                }
                isFirstButton = true;
                firstImg = null;
            }
            // disable all buttons and set a timer, then hide the images
            else
            {
                imgBtn.IsEnabled = true;
                firstButton.IsEnabled = true;
                isFirstButton = true;
                DisableAllButtons();
                await Task.Delay(levelMilliseconds);
                img.Visibility = Visibility.Hidden;
                firstImg.Visibility = Visibility.Hidden;
                for (int j = 0; j < imgBtnList.Capacity; j++)
                {
                    if (disabledButtons[j] == false) imgBtnList[j].IsEnabled = true;
                }
            }
            // check if the player matched all the tiles
            IsGameDone();
        }

        private void IsGameDone()
        {
            int counter = 0;
            foreach (var btn in disabledButtons)
            {
                if (btn) counter++;
                if (counter == 16)
                {
                    ProgressLbl.Content = "Congrats!";
                    timer.Stop();
                    StopBtn.IsEnabled = false;
                    StartBtn.IsEnabled = true;
                    levelDiffcultySlider.IsEnabled = true;
                    PlayerNameTxtBox.IsEnabled = true;
                    SaveButton.IsEnabled = true;
                }
            }
        }

        // all buttons in the grid has a click event, and in those
        // the appropriate image will display and goes to isMatching method

        private void imgBtn1_Click(object sender, RoutedEventArgs e)
        {
            img1.Visibility = Visibility.Visible;
            IsMatching(img1, imgBtn1);
        }

        private void imgBtn2_Click(object sender, RoutedEventArgs e)
        {
            img2.Visibility = Visibility.Visible;
            IsMatching(img2, imgBtn2);
        }

        private void imgBtn3_Click(object sender, RoutedEventArgs e)
        {
            img3.Visibility = Visibility.Visible;
            IsMatching(img3, imgBtn3);
        }

        private void imgBtn4_Click(object sender, RoutedEventArgs e)
        {
            img4.Visibility = Visibility.Visible;
            IsMatching(img4, imgBtn4);
        }

        private void imgBtn5_Click(object sender, RoutedEventArgs e)
        {
            img5.Visibility = Visibility.Visible;
            IsMatching(img5, imgBtn5);
        }

        private void imgBtn6_Click(object sender, RoutedEventArgs e)
        {
            img6.Visibility = Visibility.Visible;
            IsMatching(img6, imgBtn6);
        }

        private void imgBtn7_Click(object sender, RoutedEventArgs e)
        {
            img7.Visibility = Visibility.Visible;
            IsMatching(img7, imgBtn7);
        }

        private void imgBtn8_Click(object sender, RoutedEventArgs e)
        {
            img8.Visibility = Visibility.Visible;
            IsMatching(img8, imgBtn8);
        }

        private void imgBtn9_Click(object sender, RoutedEventArgs e)
        {
            img9.Visibility = Visibility.Visible;
            IsMatching(img9, imgBtn9);
        }

        private void imgBtn10_Click(object sender, RoutedEventArgs e)
        {
            img10.Visibility = Visibility.Visible;
            IsMatching(img10, imgBtn10);
        }

        private void imgBtn11_Click(object sender, RoutedEventArgs e)
        {
            img11.Visibility = Visibility.Visible;
            IsMatching(img11, imgBtn11);
        }

        private void imgBtn12_Click(object sender, RoutedEventArgs e)
        {
            img12.Visibility = Visibility.Visible;
            IsMatching(img12, imgBtn12);
        }

        private void imgBtn13_Click(object sender, RoutedEventArgs e)
        {
            img13.Visibility = Visibility.Visible;
            IsMatching(img13, imgBtn13);
        }

        private void imgBtn14_Click(object sender, RoutedEventArgs e)
        {
            img14.Visibility = Visibility.Visible;
            IsMatching(img14, imgBtn14);
        }

        private void imgBtn15_Click(object sender, RoutedEventArgs e)
        {
            img15.Visibility = Visibility.Visible;
            IsMatching(img15, imgBtn15);
        }

        private void imgBtn16_Click(object sender, RoutedEventArgs e)
        {
            img16.Visibility = Visibility.Visible;
            IsMatching(img16, imgBtn16);
        }

        private void StartBtn_Click(object sender, RoutedEventArgs e)
        {
            // if the game has already started, disable start button, and enable stop button and all buttons
            if (StopBtn.IsEnabled)
            {
                StopBtn.IsEnabled = true;
                EnableAllButtons();
            }
            // randomize the images to the buttons
            else
            {
                RandomizeGrid();
                levelDiffcultySlider.IsEnabled = false;
                TimerLbl.Content = "00:00";
                addSeconds = 1;
                ProgressLbl.Content = "";
                SaveButton.IsEnabled = false;
                PlayerNameTxtBox.IsEnabled = false;
            }
            StartBtn.IsEnabled = false;
            StopBtn.IsEnabled = true;
            // start timer
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            TimerLbl.Content = string.Format("{0:mm\\:ss}", TimeSpan.Zero.Add(TimeSpan.FromSeconds(addSeconds)));
            addSeconds++;
        }

        private void StopBtn_Click(object sender, RoutedEventArgs e)
        {
            // stop timer, enable start button and level slider, disable all buttons and images
            timer.Stop();
            StartBtn.IsEnabled = true;
            StopBtn.IsEnabled = false;
            levelDiffcultySlider.IsEnabled = true;
            DisableAllButtons();
            HideAllImages();
            ProgressLbl.Content = "";
        }

        // randomizer
        private void RandomizeGrid()
        {
            disabledButtons = new bool[16];
            // randomize the images to the buttons
            int[] imgFileTracking = new int[8];

            Random rand = new Random();
            for (int i = 0; i < imgBoxList.Capacity; i++)
            {
                imgBoxList[i].Visibility = Visibility.Hidden;
                int randomNumber = rand.Next(0, 8);
                if (imgFileTracking[randomNumber] < 2)
                {
                    imgBoxList[i].Source = imgFileList[randomNumber];
                    imgFileTracking[randomNumber]++;
                }
                // to avoid having more than two of the same image, set the counter of the loop back
                else
                {
                    i--;
                }
            }
            EnableAllButtons();
        }

        private void DisableAllButtons()
        {
            foreach (var btn in imgBtnList) btn.IsEnabled = false;
        }

        private void EnableAllButtons()
        {
            foreach (var btn in imgBtnList) btn.IsEnabled = true;
        }

        private void HideAllImages()
        {
            foreach (var img in imgBoxList) img.Visibility = Visibility.Hidden;
        }

        private void FileOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog
            {
                Title = "Open text file",
                Filter = "txt file|*.txt"
            };
            if (ofd.ShowDialog() == true)
            {
                LeaderboardLoad lb = new LeaderboardLoad(File.ReadAllLines(ofd.FileName));
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string playerScore = (string)TimerLbl.Content + ',' + PlayerNameTxtBox.Text + ',' + ((string)levelLbl.Content).Substring(7);
            File.AppendAllText("players.txt", playerScore + Environment.NewLine);
            LoadBestPlayers();
        }
    }
}
